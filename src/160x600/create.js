function setElements(callback) {

    config = {};
    config.bannerWidth = 160;
    config.bannerHeight = 600;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('regular.woff')
        ], add);
    }

    function add() {

        ___("bg").image(dd.background160x600, { width: 160, height: 600, wrap: true })
        
        ___("footer")
            .style({ width:config.bannerWidth, height:92, background:"#fff" })
            .position({ bottom:0, left:0 })

        ___("logo")
            .image(asset("logo.png"), { width:100, height:26, fit:true })
            .position({ centerX:0, bottom:55 })

        ___("h1")
            .text(dd.copy.h1, { width:114, height:70, background:"#e60167", color:"rgba(255,255,255,0)", webfont:"bold" })
            .style({ css:"padding:10px;" })
            .position({ left:13, top:163 })
            
        ___("h2")
            .text(dd.copy.h2, { width:134, height:100, maxFs:19, color:"rgba(255,255,255,1)", webfont:"bold" })
            .position({ top:20, left:13 })
        
        if(dd.copy.h3 != ""){
            ___("h3")
                .text(dd.copy.h3, { width:134, height:100, maxFs:15, color:"rgba(255,255,255,1)", webfont:"regular" })
                .position({ push:{ el:__("h2"), bottom:5 }, left:13 }) 
        }
        
        ___("gift")
            .image(asset("gift.png"), { width:140, height:140, fit:true })
            .position({ top:350, left:10 })

        ___("sticker")
            .image(dd.sticker, { width:122, height:130, fit:true })
            .position({ centerX:0, top:220 })

        ___("cta")
            .text(dd.copy.cta, { background:"#01b5ce", color:"#fff", textAlign:"center", webfont:"bold", fontSize:12 })
            .style({ css:"border-bottom:1px solid #0e8194; padding:8px 18px;" })
            .position({ bottom:15, centerX:0 })

        sr.loading.done(callback);
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);
    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;