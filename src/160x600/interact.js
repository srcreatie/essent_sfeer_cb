function animate(callback) {

    setAnimation();

    function setAnimation() {

        __("creative").style.opacity = 1;

        var tl01 = new TimelineMax();
        var tlRO = new TimelineMax({ paused:true });
        

        tl01.from(__("h1"), 1, { x:-100, alpha:0, ease:Back.easeOut.config(1.2) })
        tl01.to(__("h1"), 0.75, { css:"color:rgba(255,255,255,1)" }, "+=0.2")
        tl01.to(this, 1.5, {})
        tl01.to(__("h1"), 0.5, { x:-100, alpha:0 })
        tl01.to(__("bg"), 0.5, { alpha:0 }, "bg")
        tl01.from(__("gift"), 0.5, { alpha:0 }, "bg")
        tl01.to(this, 0.3, {})
        tl01.from(__("h2"), 0.5, { alpha:0 }, "h2")
        if(dd.copy.h3 != ""){
            tl01.from(__("h3"), 0.5, { alpha:0 }, "h2+=0.4")
        }
        
        tl01.from(__("sticker"), 0.5, { alpha:0, scale:10, rotation:20 })
        tl01.to(__("cta"), 1, { scale:1.1, yoyo:true, repeat:3, ease:Back.easeOut.config(1.2) })

        tl01.to(this, 0, { onComplete:rollOver })

        tlRO.to(__("cta"), 0.5, { scale:1.1, yoyo:true, repeat:1 })

        function rollOver() {
            __("banner").onmouseover = function() {
                tlRO.play(0);
            };
        }

        if (srBanner.debug) {
            if (srBanner.debug && srBanner.pauseFrom) {
                console.log("pause from " + srBanner.pauseFrom);
                tl01.pause(srBanner.pauseFrom);
            }

            if (srBanner.debug && srBanner.playFrom) {
                console.log("play from " + srBanner.playFrom);
                tl01.pause(srBanner.playFrom);
            }

            if (srBanner && srBanner.backupImage) {
                console.log("create backup images of last frame");
                tl01.add("screenshot");
                tl01.pause("screenshot");
            }
        }

    }

    if (callback) {

        callback();

    }

}

module.exports.animate = animate;