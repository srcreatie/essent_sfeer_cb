function setElements(callback) {

    config = {};
    config.bannerWidth = 336;
    config.bannerHeight = 280;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('regular.woff')
        ], add);
    }

    function add() {

        ___("bg").image(dd.background336x280, { width: 336, height: 280, wrap: true })
        
        ___("footer")
            .style({ width:config.bannerWidth, height:52, background:"#fff" })
            .position({ bottom:0, left:0 })

        ___("logo")
            .image(asset("logo.png"), { width:95, height:24, fit:true })
            .position({ left:15, bottom:14 })

        ___("h1")
            .text(dd.copy.h1, { width:200, height:70, background:"#e60167", color:"rgba(255,255,255,0)", webfont:"bold" })
            .style({ css:"padding:10px;" })
            .position({ left:17, bottom:68 })
            
        ___("h2")
            .text(dd.copy.h2, { width:170, height:100, maxFs:22, color:"rgba(255,255,255,1)", webfont:"bold" })
            .position({ top:20, left:20 })
        
        if(dd.copy.h3 != ""){
            ___("h3")
                .text(dd.copy.h3, { width:170, height:100, maxFs:17, color:"rgba(255,255,255,1)", webfont:"regular" })
                .position({ push:{ el:__("h2"), bottom:5 }, left:20 }) 
        }
        
        ___("gift")
            .image(asset("gift.png"), { width:113, height:113, fit:true })
            .position({ left:170, top:100 })

        ___("sticker")
            .image(dd.sticker, { width:100, height:110, fit:true })
            .position({ left:210, top:8 })

        ___("cta")
            .text(dd.copy.cta, { background:"#01b5ce", color:"#fff", textAlign:"center", webfont:"bold", fontSize:14 })
            .style({ css:"border-bottom:1px solid #0e8194; padding:8px 18px;" })
            .position({ bottom:10, right:15 })

        sr.loading.done(callback);
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);
    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;