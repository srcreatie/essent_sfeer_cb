function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('regular.woff')
        ], add);
    }

    function add() {

        ___("bg").image(dd.background300x250, { width: 300, height: 250, wrap: true })
        
        ___("footer")
            .style({ width:config.bannerWidth, height:42, background:"#fff" })
            .position({ bottom:0, left:0 })

        ___("logo")
            .image(asset("logo.png"), { width:80, height:20, fit:true })
            .position({ left:10, bottom:11 })

        ___("h1")
            .text(dd.copy.h1, { width:155, height:45, background:"#e60167", color:"rgba(255,255,255,0)", webfont:"bold" })
            .style({ css:"padding:10px;" })
            .position({ left:17, bottom:58 })
            
        ___("h2")
            .text(dd.copy.h2, { width:145, height:100, maxFs:20, color:"rgba(255,255,255,1)", webfont:"bold" })
            .position({ top:20, left:20 })
        
        if(dd.copy.h3 != ""){
            ___("h3")
                .text(dd.copy.h3, { width:125, height:100, maxFs:15, color:"rgba(255,255,255,1)", webfont:"regular" })
                .position({ push:{ el:__("h2"), bottom:5 }, left:20 }) 
        }
        
        ___("gift")
            .image(asset("gift.png"), { width:113, height:113, fit:true })
            .position({ left:150, top:90 })

        ___("sticker")
            .image(dd.sticker, { width:100, height:110, fit:true })
            .position({ left:190, top:8 })

        ___("cta")
            .text(dd.copy.cta, { background:"#01b5ce", color:"#fff", textAlign:"center", webfont:"bold", fontSize:11 })
            .style({ css:"border-bottom:1px solid #0e8194; padding:8px 18px;" })
            .position({ bottom:7, right:10 })

        sr.loading.done(callback);
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);
    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;