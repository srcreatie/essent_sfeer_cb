function setElements(callback) {

    config = {};
    config.bannerWidth = 728;
    config.bannerHeight = 90;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('regular.woff')
        ], add);
    }

    function add() {

        ___("bg").image(dd.background728x90, { width: 728, height: 90, wrap: true })
        
        ___("footer")
            .style({ width:140, height:90, background:"#fff" })
            .position({ bottom:0, right:0 })

        ___("logo")
            .image(asset("logo.png"), { width:95, height:24, fit:true })
            .position({ right:20, top:11 })

        ___("h1")
            .text(dd.copy.h1, { width:155, height:45, background:"#e60167", color:"rgba(255,255,255,0)", webfont:"bold" })
            .style({ css:"padding:10px;" })
            .position({ left:30, top:12 })
            
        ___("h2")
            .text(dd.copy.h2, { width:300, height:50, maxFs:20, color:"rgba(255,255,255,1)", webfont:"bold" })
        
            if(dd.copy.h3 != ""){
                ___("h2")
                    .position({ top:14, left:20 })
            }else{
                ___("h2")
                    .position({ left:20, centerY:0 })
            }
        
        if(dd.copy.h3 != ""){
            ___("h3")
                .text(dd.copy.h3, { width:300, height:25, maxFs:15, color:"rgba(255,255,255,1)", webfont:"regular" })
                .position({ push:{ el:__("h2"), bottom:5 }, left:20 }) 
        }
        
        ___("gift")
            .image(asset("gift.png"), { width:113, height:113, fit:true })
            .position({ left:330, top:10 })

        ___("sticker")
            .image(dd.sticker, { width:82, height:88, fit:true })
            .position({ left:445, top:6 })

        ___("cta")
            .text(dd.copy.cta, { background:"#01b5ce", color:"#fff", textAlign:"center", webfont:"bold", fontSize:12 })
            .style({ css:"border-bottom:1px solid #0e8194; padding:8px 18px;" })
            .position({ bottom:12, right:12 })

        sr.loading.done(callback);
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);
    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;