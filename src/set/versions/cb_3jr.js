function setData(callback) {

    var devDynamicContent = {};

    devDynamicContent.srFeed = [{}];
    devDynamicContent.srFeed[0]._id = 0;
    devDynamicContent.srFeed[0].isWorking = "Default feed 2";
    devDynamicContent.srFeed[0].exit_url = "";
    devDynamicContent.srFeed[0].copy = {};
    devDynamicContent.srFeed[0].copy.h1 = "Maak het thuis... ...voordeliger"
    devDynamicContent.srFeed[0].copy.h2 = "Met 3 jaar het állerlaagste tarief én € 100,- cashback"
    devDynamicContent.srFeed[0].copy.h3 = ""
    devDynamicContent.srFeed[0].copy.cta = "Bekijk aanbod"
    devDynamicContent.srFeed[0].sticker = asset("3jr_sticker.png");
    devDynamicContent.srFeed[0].background160x600 = dimension("3jr_160x600.jpg", "160x600");
    devDynamicContent.srFeed[0].background300x250 = dimension("3jr_300x250.jpg", "300x250");
    devDynamicContent.srFeed[0].background336x280 = dimension("3jr_336x280.jpg", "336x280");
    devDynamicContent.srFeed[0].background728x90 = dimension("3jr_728x90.jpg", "728x90");
    devDynamicContent.srFeed[0].background970x250 = dimension("3jr_970x250.jpg", "970x250");
    devDynamicContent.srFeed[0].background300x600 = dimension("3jr_300x600.jpg", "300x600");

    return devDynamicContent;
}

module.exports = setData;