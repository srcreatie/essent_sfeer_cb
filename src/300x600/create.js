function setElements(callback) {

    config = {};
    config.bannerWidth = 300;
    config.bannerHeight = 600;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('regular.woff')
        ], add);
    }

    function add() {

        ___("bg").image(dd.background300x600, { width: 300, height: 600, wrap: true })
        
        ___("footer")
            .style({ width:config.bannerWidth, height:54, background:"#fff" })
            .position({ bottom:0, left:0 })

        ___("logo")
            .image(asset("logo.png"), { width:107, height:25, fit:true })
            .position({ left:17, bottom:14 })

        ___("h1")
            .text(dd.copy.h1, { width:235, height:80, background:"#e60167", color:"rgba(255,255,255,0)", webfont:"bold" })
            .style({ css:"padding:10px;" })
            .position({ left:17, bottom:75 })
            
        ___("h2")
            .text(dd.copy.h2, { width:260, height:100, maxFs:29, color:"rgba(255,255,255,1)", webfont:"bold" })
            .position({ top:20, left:20 })
        
        if(dd.copy.h3 != ""){
            ___("h3")
                .text(dd.copy.h3, { width:260, height:100, maxFs:18, color:"rgba(255,255,255,1)", webfont:"regular" })
                .position({ push:{ el:__("h2"), bottom:5 }, left:20 }) 
        }
        
        ___("gift")
            .image(asset("gift.png"), { width:220, height:220, fit:true })
            .position({ centerX:15, bottom:70 })

        ___("sticker")
            .image(dd.sticker, { width:160, height:165, fit:true })
            .position({ left:60, top:155 })

        ___("cta")
            .text(dd.copy.cta, { background:"#01b5ce", color:"#fff", textAlign:"center", webfont:"bold", fontSize:13 })
            .style({ css:"border-bottom:1px solid #0e8194; padding:8px 18px;" })
            .position({ bottom:11, right:17 })

        sr.loading.done(callback);
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);
    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;