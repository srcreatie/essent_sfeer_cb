function setElements(callback) {

    config = {};
    config.bannerWidth = 970;
    config.bannerHeight = 250;

    addClicktag(dd.exit_url);
    loadFont();

    function loadFont() {
        sr.font.add([asset('bold.woff'),
            asset('regular.woff')
        ], add);
    }

    function add() {

        ___("bg").image(dd.background970x250, { width: 970, height: 250, wrap: true })
        
        ___("footer")
            .style({ width:294, height:250, background:"#fff" })
            .position({ bottom:0, right:0 })

        ___("logo")
            .image(asset("logo.png"), { width:182, height:36, align:"center center", fit:true })
            .position({ el:__("footer"), centerX:0, top:62 })

        ___("h1")
            .text(dd.copy.h1, { width:300, height:115, background:"#e60167", color:"rgba(255,255,255,0)", webfont:"bold" })
            .style({ css:"padding:10px;" })
            .position({ left:30, bottom:30 })
            
        ___("h2")
            .text(dd.copy.h2, { width:250, height:150, maxFs:34, color:"rgba(255,255,255,1)", webfont:"bold" })
            .position({ top:30, left:30 })
                
        if(dd.copy.h3 != ""){
            ___("h3")
                .text(dd.copy.h3, { width:250, height:100, maxFs:24, color:"rgba(255,255,255,1)", webfont:"regular" })
                .position({ push:{ el:__("h2"), bottom:4 }, left:30 }) 
        }
        
        ___("gift")
            .image(asset("gift.png"), { width:200, height:200, fit:true })
            .position({ left:300, top:40 })

        ___("sticker")
            .image(dd.sticker, { width:150, height:154, fit:true })
            .position({ left:480, top:20 })

        ___("cta")
            .text(dd.copy.cta, { background:"#01b5ce", color:"#fff", textAlign:"center", webfont:"bold", fontSize:22 })
            .style({ css:"border-bottom:1px solid #0e8194; padding:15px 33px;" })
            .position({ bottom:45, el:__("footer"), centerX:0 })

        sr.loading.done(callback);
    }
}

function addClicktag(customUrl) {
    document.getElementById("bg-exit").addEventListener('click', bgExitHandler, false);
    function bgExitHandler(e) {
        if (srBanner.dcs) {
            Enabler.exit('Background Exit');
        } else {
            window.open(clickTag);
        }
    }
}

module.exports.setElements = setElements;